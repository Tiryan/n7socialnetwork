# Choix technique pour l'application "Réseaux social N7"

Pour cette application, il sera néccessaire de réaliser : 

* Une messagerie instantannée
* Un gestionnaire de fichier (type WebDAV ou SFTP)
* Un appel vocal
* Un mur pour faire des demandes globales
* Un appel visio ?

Pour chaque personne, il aura la possibilité d'appartenir à des groupes et des sous-groupes, par exemple groupe classe avec sous-groupes équipe de projet.

Il faudra dimensionner le serveur en fonction de l'affluence de message mais aussi du nombre de fichiers à conserver (demander à l'ENSEEIHT d'avoir accés à leur serveur ?)

Langage à utiliser : 
    NodeJS pour la partie backend du site web
    Pas encore définis pour la partie Frontend
    Base de données MySQL pour la partie donnée
    ElectronJS pour la partie desktop
    Kotlin pour Android

